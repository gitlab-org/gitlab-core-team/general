# frozen_string_literal: true

require 'date'

module Helper
  def find_third_tuesday
    # Get the first day of the month
    date = Date.new(Date.today.year, Date.today.month, 1)

    # Go to beginning of next month
    date = date.next_month

    # Tuesday is the second day of the week, so get the delta from
    # the first day of the month to the first Tuesday of the month and add it
    date += (2 - date.wday) % 7

    # Add 14 days, two weeks, to get the third Tuesday of the month
    date += 14

    date
  end

  def is_meeting_day?
    Date.today.wday == 2 && Date.today.mday >= 15 && Date.today.mday <= 21
  end
end

Gitlab::Triage::Resource::Context.include Helper
