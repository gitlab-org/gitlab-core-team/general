<!-- Note: Any Core Team member or GitLab Team member can nominate anyone from the wider community at any time -->

## Nominee Details

**Nominee GitLab Username**: [`@username`](https://GitLab.com/username)

## Contribution Overview

* **Duration of Engagement with GitLab**:
  <!-- For example: "2 years" or "Since January 2021" -->

* **Contribution Areas**:
  * [ ] Development
  * [ ] Documentation
  * [ ] Community Building & Engagement
  * [ ] Forum Participation
  * [ ] Translations
  * [ ] UX Design
  * [ ] Mentorship & Guidance
  * [ ] Recognized as MVP or even nominated
  * [ ] Other: _______

* **Notable Contributions**:
  <!-- Describe or link to the nominee's most impactful contributions, including a couple of MRs that stand out. -->

* **Merge Requests**:
  * [View All MRs by `@username`](https://gitlab.com/dashboard/merge_requests?scope=all&state=all&author_username=username)
  * [View MRs by `@username` with `Community contribution` label](https://gitlab.com/dashboard/merge_requests?scope=all&state=all&author_username=username&label_name[]=Community%20contribution)

* **Issues Engaged With**:
  * [View All Issues for `@username`](https://gitlab.com/dashboard/issues?scope=all&state=all&author_username=username)
  * [View Authored Issues from `@username` in gitlab-org](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=all&author_username=username)
  * [View Authored Issues from `@username` in gitlab-com](https://gitlab.com/groups/gitlab-com/-/issues?scope=all&state=all&author_username=username)

* **Comments and Discussions**:
  * [Search for comments by `@username` in gitlab-org group](https://gitlab.com/search?group_id=9970&scope=notes&search=username)
  * [Search for comments by `@username` in gitlab-com group](https://gitlab.com/search?group_id=6543&scope=notes&search=username)
    <!-- This search query provides insights into the nominee's engagement with the team and wider community through their comments in discussions within the gitlab-org group. -->

### Additional Criteria for Consideration

- **Act as a steward for the wider GitLab community**
  <!-- Example: "Actively engaged with new contributors on multiple MRs, guiding them through the contribution process." -->

- **Alignment with Core Team's Mission**
  <!-- Provide specific examples that showcase how the nominee aligns with the Core Team's mission. -->

- **Help GitLab live up to its mission**
  <!-- Example: "Developed a feature that significantly improves CI/CD process, directly contributing to GitLab's mission of everyone can contribute." -->

- **Help GitLab live up to its values**
  <!-- Example: "Regularly provides constructive and transparent feedback in MR reviews, aligning with GitLab's value of Collaboration." -->

/confidential 
/assign @gitlab-org/gitlab-core-team 
