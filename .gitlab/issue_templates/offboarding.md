<!-- Issue title: @username offboarding issue -->

## Existing Core team member

- [ ] Move the individual from the team.yml file to the `data/inactive_core_team.yml`: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/136165
- [ ] Remove from https://gitlab.com/groups/gitlab-org/gitlab-core-team/-/group_members
- [ ] Remove from https://gitlab.com/groups/gitlab-org/gitlab-core-team/community-members/-/group_members
- [ ] Ask a [forum admin](https://handbook.gitlab.com/handbook/marketing/developer-relations/workflows-tools/forum/#administration) to remove the core team member from the [core team](https://forum.gitlab.com/g/core-team) group on the forum.

## GitLab team member

- [ ] Remove the "GitLab Core Team" role if the core team member is a member on the community discord
- [ ] Slack access removal request: https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/31552
- [ ] `gitlab-org` access removal request: https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/31553
- [ ] Award Core team (inactive) achievement:
  ```graphql
  mutation  {
    achievementsAward(input: {
      achievementId: "gid://gitlab/Achievements::Achievement/1000065"
      userId: "gid://gitlab/User/"
    }) {
      errors
    }
  }
  ```
 
## gitlab-org maintainer (`@rymai`)

- [ ] Revoke Core team achievement:
  ```graphql
  mutation {
    achievementsRevoke(input: {
      userAchievementId: "gid://gitlab/Achievements::UserAchievement/"
    }) {
      errors
    }
  }
  ```
