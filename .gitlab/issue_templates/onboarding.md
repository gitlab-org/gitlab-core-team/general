<!-- Issue title: @username onboarding issue -->

## New Core team member

- [ ] Sign [NDA](https://docs.google.com/document/d/1hPmHmU3fIhO3vsDXU4Mjjuuvid0Z-scl/edit?usp=share_link&ouid=111234761090585401601&rtpof=true&sd=true)(only accessible by GitLab team members)
- [ ] Team member page: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/119671

## Existing Core team member

- [ ] Add as `owner` to https://gitlab.com/groups/gitlab-org/gitlab-core-team/-/group_members
- [ ] Add as `owner` to https://gitlab.com/groups/gitlab-org/gitlab-core-team/community-members/-/group_members
- [ ] Ask a [forum admin](https://handbook.gitlab.com/handbook/marketing/developer-relations/workflows-tools/forum/#administration) to add the new core team member to the [core team](https://forum.gitlab.com/g/core-team) group on the forum.

## GitLab team member

- [ ] Assign the "GitLab Core Team" role if the new core team member is a member on the community discord
- [ ] Slack access request: https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/21504
- [ ] `gitlab-org` access request: https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/20681
- [ ] Slack intro in `#whats-happening-at-gitlab`
 
## gitlab-org maintainer (`@rymai`)

- [ ] Award Core team achievement:
  ```graphql
  mutation {
    achievementsAward(input: {
      achievementId: "gid://gitlab/Achievements::Achievement/1",
      userId: "gid://gitlab/User/" }) {
      userAchievement {
        id
      }
      errors
    }
  }
  ```
  
